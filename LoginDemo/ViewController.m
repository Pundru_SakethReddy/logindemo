//
//  ViewController.m
//  LoginDemo
//
//  Created by Saketh Reddy on 02/03/17.
//  Copyright © 2017 Saketh Reddy. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        SecondViewController *svc = [[SecondViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
    }
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileUpdated:) name:FBSDKProfileDidChangeNotification object:nil];
    
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    // Uncomment to automatically sign in the user.
    //[[GIDSignIn sharedInstance] signInSilently];
    
    
    
    if ([GIDSignIn sharedInstance].hasAuthInKeychain) {
        NSLog(@"Signed in");
        [[GIDSignIn sharedInstance] signInSilently];
        SecondViewController *svc = [[SecondViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
        
    } else {
        NSLog(@"Not signed in");
    }
    
    
    
    
    
    //    ViewController *AppDelegate = (ViewController *)[[UIApplication sharedApplication] delegate];
    
    
}

-(void)profileUpdated:(NSNotification *)note{
    
    if ([FBSDKAccessToken currentAccessToken] != nil) {
        
        NSString *name = [FBSDKProfile.currentProfile name];
        NSString *uid  = [FBSDKProfile.currentProfile userID];
        NSString *fname = [FBSDKProfile.currentProfile firstName];
        NSString *lname = [FBSDKProfile.currentProfile lastName];
        NSURL *pplink = [FBSDKProfile.currentProfile imageURLForPictureMode:FBSDKProfilePictureModeSquare size:CGSizeMake(400, 400)];
        
        NSLog(@"%@",name);
        NSLog(@"%@",uid);
        NSLog(@"%@",fname);
        NSLog(@"%@",lname);
        NSLog(@"%@",pplink);
        
        
    }
}



//-(void)loginButtonClicked{
//
////    if ([FBSDKAccessToken currentAccessToken] == nil) {
//        //    }
//}

-(void)getEmail{
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"email" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSLog(@"fetched user:%@", result);
         }
     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginClick:(id)sender {
    
    if ([FBSDKAccessToken currentAccessToken]==nil) {
        FBSDKLoginManager *fbLogin = [[FBSDKLoginManager alloc]init];
        [fbLogin logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error) {
                NSLog(@"Process Error");
            } else if (result.isCancelled) {
                NSLog(@"Cancelled");
            }else{
                NSLog(@"Logged in");
                
                [self getEmail];
                SecondViewController *svc = [[SecondViewController alloc]init];
                [self.navigationController pushViewController:svc animated:YES];
                
            }
        }];
        
    }else{
        
        SecondViewController *svc = [[SecondViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
    }
    
    
}

@end
