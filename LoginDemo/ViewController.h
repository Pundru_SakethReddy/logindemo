//
//  ViewController.h
//  LoginDemo
//
//  Created by Saketh Reddy on 02/03/17.
//  Copyright © 2017 Saketh Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import "AppDelegate.h"

@interface ViewController : UIViewController<GIDSignInUIDelegate>

@property (strong, nonatomic) IBOutlet GIDSignInButton *signInButton;

//- (IBAction)loginClick:(id)sender;

- (IBAction)loginClick:(id)sender;



@end

