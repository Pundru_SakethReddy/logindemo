//
//  AppDelegate.h
//  LoginDemo
//
//  Created by Saketh Reddy on 02/03/17.
//  Copyright © 2017 Saketh Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Google/SignIn.h>
#import <OIDAuthorizationService.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;



- (void)saveContext;


@end

