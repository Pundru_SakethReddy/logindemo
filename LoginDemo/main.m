//
//  main.m
//  LoginDemo
//
//  Created by Saketh Reddy on 02/03/17.
//  Copyright © 2017 Saketh Reddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
